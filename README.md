# MeetKatowice.eu backend

## Stack
This app was built for **Node 12 LTS** and **MongoDB 4**.

## Run
To run this server just run `npm start` command.

For development run, that is pretty logs in console and server reload at file change run `npm run-script dev`.

#### `.env` file
Proper enviorment setting is required for this app to run. Just create a `.env` file in the root directory (where's the app.js/server.js file).

After that, this `.env` file must have the following entries:
* *PORT* - this is a port number that the server will listen for reqeuests at,
* *DB_NAME* - name of the database,
* *DB_ADDRESS* - address of the database (address should contain port number),
* *DB_AUTH_COMBO* - username and password in specific pattern, that is: `"username:password"` for the database. If you're running databse in a local enviroment and you does not have a username and password auth configured just put a blank string there: `""`,
* *SESSION_SECRET* - session secret. Must be at least 32 characters long,

Here's a example configuration file:
```env
PORT=80
DB_NAME="meetkatowice"
DB_ADDRESS="localhost:27017"
DB_AUTH_COMBO="admin:admin"
SESSION_SECRET="pkGLwLaCG4SZHmr8QSI"
```

### Running without fastify-cli
The app can be run with `node server.js` command too. It uses `dotenv` library to load a configuration variables, but it also has a default settings as a fallback.
