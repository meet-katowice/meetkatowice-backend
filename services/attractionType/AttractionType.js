const ObjectId = require('mongodb').ObjectID
const collection = 'attractionTypes'

async function validateAttractionType (db, attractionType) {
  if (await findByName(db, attractionType.name) !== null) {
    throw new Error('Attraction type with given name already exists')
  }
  if (await findByKind(db, attractionType.kind) !== null) {
    throw new Error('Attraction type with given kind already exists')
  }

  return true
}
module.exports.validateAttractionType = validateAttractionType

async function createAttractionType (db, attractionType) {
  const result = await db.collection(collection).insertOne(attractionType)
  return result
}
module.exports.create = createAttractionType

async function findById (db, id) {
  return db.collection(collection).findOne({ _id: ObjectId(id) })
}
module.exports.findById = findById

async function findByName (db, name) {
  return db.collection(collection).findOne({ name })
}
module.exports.findByName = findByName

async function findByKind (db, kind) {
  return db.collection(collection).findOne({ kind })
}
module.exports.findByKind = findByKind

async function getAll (db) {
  return db.collection(collection).find({}).toArray()
}
module.exports.getAll = getAll

async function update (db, attractionType, id) {
  return db.collection(collection).updateOne({ _id: ObjectId(id) }, { $set: { ...attractionType } })
}
module.exports.update = update
