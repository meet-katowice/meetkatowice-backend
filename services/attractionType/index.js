module.exports = async function (app, opts) {
  app.register(require('./createHandler'))
  app.register(require('./readHandler'))
  app.register(require('./updateHandler'))
}

module.exports.autoPrefix = '/api/attractionType'
