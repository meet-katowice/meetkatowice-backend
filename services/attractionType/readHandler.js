const schema = require('./schema')
const AttractionType = require('./AttractionType')

module.exports = async function (app, opts) {
  app.get('/:attractionTypeId', { schema: schema.attractionTypeSchema }, async function (request, reply) {
    await findAttractionByIdAndSendReply(app.mongo.db, reply, request.params.attractionTypeId)
  })

  app.get('/', { schema: schema.attractionTypesListSchema }, async function (request, reply) {
    await getAllAttractionTypesAndSendReply(app.mongo.db, reply)
  })
}

async function findAttractionByIdAndSendReply (db, reply, id) {
  const attractionType = await AttractionType.findById(db, id)
  if (!attractionType) {
    return reply.notFound()
  }
  reply.send(attractionType)
}

async function getAllAttractionTypesAndSendReply (db, reply) {
  const attractionTypes = await AttractionType.getAll(db)
  reply.send(attractionTypes)
}
