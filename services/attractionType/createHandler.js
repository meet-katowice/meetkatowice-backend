const schema = require('./schema')
const AttractionType = require('./AttractionType')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'attractionTypes', requiredPermissions: ['C'] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.post('/add', { schema: schema.createAttractionTypeSchema }, async function (request, reply) {
      await createAttractionTypeAndSendReply(app.mongo.db, reply, request.body)
    })
  })
}

async function createAttractionTypeAndSendReply (db, reply, attractionType) {
  try {
    await AttractionType.validateAttractionType(db, attractionType)
  } catch (e) {
    reply.code(409)
    return reply.send(e)
  }

  const createdAttractionType = await AttractionType.create(db, attractionType)
  reply.code(201).send({ ...attractionType, _id: createdAttractionType.insertedId })
}
