const schema = require('./schema')
const AttractionType = require('./AttractionType')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'attractionTypes', requiredPermissions: ['U'] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.put('/update/:attractionTypeId', { schema: schema.updateAttractionTypeSchema }, async function (request, reply) {
      await updateAttractionTypeAndSendReply(app.mongo.db, reply, request.body, request.params.attractionTypeId)
    })
  })
}

async function updateAttractionTypeAndSendReply (db, reply, attractionType, id) {
  if (Object.keys(attractionType).length === 0) {
    return reply.badRequest()
  }

  const currentAttractionType = await AttractionType.findById(db, id)
  if (!currentAttractionType) {
    return reply.notFound()
  }

  try {
    await AttractionType.validateAttractionType(db, attractionType)
  } catch (e) {
    reply.code(409)
    return reply.send(e)
  }

  await AttractionType.update(db, attractionType, id)
  reply.code(201).send({ ...currentAttractionType, ...attractionType, _id: id })
}
