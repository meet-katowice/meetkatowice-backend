const objectIdRegExp = '^[A-Fa-f0-9]{24}$'

const attractionTypeSchema = {
  type: 'object',
  properties: {
    _id: {
      type: 'string'
    },
    name: {
      type: 'string'
    },
    kind: {
      type: 'string'
    }
  },
  additionalProperties: false
}

const attractionTypesListSchema = {
  type: 'array',
  items: attractionTypeSchema
}

module.exports = {
  attractionTypeSchema: {
    params: {
      attractionTypeId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    response: {
      200: attractionTypeSchema
    }
  },
  attractionTypeBodySchema: attractionTypeSchema,
  attractionTypesListSchema: {
    response: {
      200: attractionTypesListSchema
    }
  }
}
