const objectIdRegExp = '^[A-Fa-f0-9]{24}$'

const updateAttractionTypeSchema = {
  type: 'object',
  properties: {
    name: {
      type: 'string',
      maxLength: 100,
      minLength: 2
    },
    kind: {
      type: 'string',
      maxLength: 100,
      minLength: 2
    }
  },
  additionalProperties: false,
  minProperties: 1
}

module.exports = {
  updateAttractionTypeSchema: {
    params: {
      attractionTypeId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    body: updateAttractionTypeSchema,
    response: {
      201: require('./read').attractionTypeBodySchema
    }
  }
}
