const objectIdRegExp = '^[A-Fa-f0-9]{24}$'

module.exports = {
  deleteAttractionTypeSchema: {
    params: {
      attractionTypeId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    response: {
      200: require('./read').attractionTypeBodySchema
    }
  }
}
