const createAttractionTypeSchema = {
  type: 'object',
  properties: {
    name: {
      type: 'string',
      maxLength: 100,
      minLength: 2
    },
    kind: {
      type: 'string',
      maxLength: 100,
      minLength: 2
    }
  },
  additionalProperties: false,
  required: ['name', 'kind']
}

module.exports = {
  createAttractionTypeSchema: {
    body: createAttractionTypeSchema,
    response: {
      201: require('./read').attractionTypeBodySchema
    }
  }
}
