'use strict'

module.exports.schema = {
  response: {
    200: {
      type: 'object',
      properties: {
        user: {
          type: 'object',
          properties: {
            _id: {
              type: 'string'
            },
            username: {
              type: 'string'
            },
            permissions: {
              type: 'array'
            }
          }
        }
      }
    }
  }
}
