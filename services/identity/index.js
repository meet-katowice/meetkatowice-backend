'use strict'

const schema = require('./schema')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'users', requiredPermissions: [] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.get('/me', { schema }, async function (request, reply) {
      reply.send({
        user: request.session.user
      })
    })
  })
}

module.exports.autoPrefix = '/api'
