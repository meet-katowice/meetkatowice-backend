const ObjectId = require('mongodb').ObjectID
const User = require('../user/User')
const BadWords = require('bad-words')
const badWordsFilter = new BadWords()
const collection = 'comments'

async function validateComment (db, comment) {
  await validateContent(comment)
  const resourceIsValid = await validateResource(db, comment)
  if (resourceIsValid !== true) throw new Error(resourceIsValid)

  return true
}
module.exports.validateComment = validateComment

async function validateContent (comment) {
  const content = badWordsFilter.clean(comment.content)
  comment.content = content
  return true
}

async function validateResource (db, comment) {
  if (!comment.refersTo) return true

  const { resource, resourceId } = comment.refersTo
  const resourceData = await getResource(db, comment.refersTo)
  if (!resourceData) return Error(`There's no ${resource} with id ${resourceId}`)

  return true
}

async function createComment (db, comment, authorId) {
  comment.authorId = authorId
  comment.added = new Date()
  comment.refersTo.resourceId = ObjectId(comment.refersTo.resourceId)
  const result = await db.collection(collection).insertOne(comment)
  return result
}
module.exports.create = createComment

async function findById (db, id) {
  const comment = await db.collection(collection).findOne({ _id: ObjectId(id) })
  if (!comment) return null

  comment.author = await User.findById(db, comment.authorId)
  return comment
}
module.exports.findById = findById

async function findRelatedTo (db, refersTo) {
  const { resource, resourceId } = refersTo
  const resourceData = await getResource(db, refersTo)
  if (!resourceData) return null

  const comments = resource !== 'user'
    ? await db.collection(collection)
      .find({
        'refersTo.resource': resource,
        'refersTo.resourceId': ObjectId(resourceId)
      }).toArray()
    : await db.collection(collection)
      .find({
        authorId: ObjectId(resourceId)
      }).toArray()
  const resolvingResults = await resolveComments(db, comments)
  comments.forEach((comment, index) => {
    comment.author = resolvingResults[index].author
    comment.haveRelatedComments = resolvingResults[index].haveRelatedComments
  })

  return comments
}
module.exports.findRefferingTo = findRelatedTo

async function update (db, comment, id) {
  return db.collection(collection).updateOne({ _id: ObjectId(id) }, { $set: { ...comment } })
}
module.exports.update = update

async function remove (db, id) {
  const relatedComments = await findRelatedTo(db, { resource: 'comment', resourceId: id })
  for await (const relatedComment of relatedComments) {
    await remove(db, relatedComment._id)
  }
  await db.collection(collection).deleteOne({ _id: ObjectId(id) })
}
module.exports.remove = remove

async function resolveComments (db, comments) {
  const tasks = comments.map((comment) => new Promise((resolve, reject) => {
    Promise.all([
      User.findById(db, comment.authorId),
      findRelatedTo(db, { resource: 'comment', resourceId: comment._id })
    ])
      .then((results) => resolve({
        author: results[0],
        haveRelatedComments: results[1].length > 0
      }))
  }))
  return Promise.all(tasks)
}

async function getResource (db, refersTo) {
  const { resource, resourceId } = refersTo
  const resourceData = await db.collection(`${resource}s`).findOne({ _id: ObjectId(resourceId) })
  if (!resourceData) return null
  else return resourceData
}
