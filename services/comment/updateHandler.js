const schema = require('./schema')
const Comment = require('./Comment')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'comments', requiredPermissions: ['U'] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.put('/update/:commentId', { schema: schema.updateCommentSchema }, async function (request, reply) {
      await updateCommentAndSendReply(app.mongo.db, reply, request.body, request.params.commentId)
    })
  })
}

async function updateCommentAndSendReply (db, reply, comment, id) {
  if (Object.keys(comment).length === 0) {
    return reply.badRequest()
  }

  const currentComment = await Comment.findById(db, id)
  if (!currentComment) {
    return reply.notFound()
  }

  try {
    await Comment.validateComment(db, comment)
  } catch (e) {
    reply.code(409)
    return reply.send(e)
  }

  await Comment.update(db, comment, id)
  reply.code(201).send({ ...currentComment, ...comment, _id: id })
}
