const schema = require('./schema')
const Comment = require('./Comment')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'comments', requiredPermissions: ['C'] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.post('/add', { schema: schema.createCommentSchema }, async function (request, reply) {
      await createCommentAndSendReply(app.mongo.db, reply, request.session.user._id, request.body)
    })
  })
}

async function createCommentAndSendReply (db, reply, authorId, comment) {
  try {
    await Comment.validateComment(db, comment)
  } catch (e) {
    reply.code(409)
    return reply.send(e)
  }

  const createdAttraction = await Comment.create(db, comment, authorId)
  reply.code(201).send({ ...comment, _id: createdAttraction.insertedId })
}
