const objectIdRegExp = '^[A-Fa-f0-9]{24}$'

const commentSchema = {
  type: 'object',
  properties: {
    _id: {
      type: 'string'
    },
    content: {
      type: 'string'
    },
    refersTo: {
      type: 'object',
      properties: {
        resource: {
          type: 'string'
        },
        resourceId: {
          type: 'string'
        }
      },
      additionalProperties: false
    },
    added: {
      type: 'string'
    },
    authorId: {
      type: 'string'
    },
    author: require('../../user/schema/read').userBodySchema,
    haveRelatedComments: {
      type: 'boolean'
    }
  },
  additionalProperties: false
}

const commentListSchema = {
  type: 'array',
  items: commentSchema
}

module.exports = {
  commentSchema: {
    params: {
      commentId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    response: {
      200: commentSchema
    }
  },
  commentBodySchema: commentSchema,
  commentListSchema: {
    params: {
      resource: {
        type: 'string',
        pattern: '^(attraction|event|comment|user)$'
      },
      resourceId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    response: {
      200: commentListSchema
    }
  }
}
