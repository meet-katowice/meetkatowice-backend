const objectIdRegExp = '^[A-Fa-f0-9]{24}$'

module.exports = {
  deleteCommentSchema: {
    params: {
      commentId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    response: {
      200: require('./read').commentBodySchema
    }
  }
}
