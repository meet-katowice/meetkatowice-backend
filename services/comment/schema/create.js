const objectIdRegExp = '^[A-Fa-f0-9]{24}$'

const createCommentSchema = {
  type: 'object',
  properties: {
    content: {
      type: 'string',
      minLength: 1,
      maxLength: 512
    },
    refersTo: {
      type: 'object',
      properties: {
        resource: {
          type: 'string',
          pattern: '^(event|attraction|comment)$'
        },
        resourceId: {
          type: 'string',
          pattern: objectIdRegExp
        }
      },
      additionalProperties: false,
      required: ['resource', 'resourceId']
    }
  },
  additionalProperties: false,
  required: ['content', 'refersTo']
}

module.exports = {
  createCommentSchema: {
    body: createCommentSchema,
    response: {
      201: require('./read').commentBodySchema
    }
  }
}
