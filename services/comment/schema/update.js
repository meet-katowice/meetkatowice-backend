const objectIdRegExp = '^[A-Fa-f0-9]{24}$'

const updateCommentSchema = {
  type: 'object',
  properties: {
    content: {
      type: 'string',
      minLength: 1,
      maxLength: 512
    }
  },
  additionalProperties: false,
  minProperties: 1
}

module.exports = {
  updateCommentSchema: {
    params: {
      commentId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    body: updateCommentSchema,
    response: {
      201: require('./read').commentBodySchema
    }
  }
}
