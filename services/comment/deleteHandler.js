const schema = require('./schema')
const Comment = require('./Comment')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'comments', requiredPermissions: ['D'] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.delete('/:commentId', { schema: schema.deleteCommentSchema }, async function (request, reply) {
      await deleteCommentAndSendReply(app.mongo.db, reply, request.params.commentId)
    })
  })
}

async function deleteCommentAndSendReply (db, reply, id) {
  const currentComment = await Comment.findById(db, id)
  if (!currentComment) {
    return reply.notFound()
  }

  await Comment.remove(db, id)
  reply.code(200).send({ ...currentComment, _id: id })
}
