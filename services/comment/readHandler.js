const schema = require('./schema')
const Comment = require('./Comment')

module.exports = async function (app, opts) {
  app.get('/:commentId', { schema: schema.commentSchema }, async function (request, reply) {
    await findCommentByIdAndSendReply(app.mongo.db, reply, request.params.commentId)
  })

  app.get('/:resource/:resourceId', { schema: schema.commentListSchema }, async function (request, reply) {
    await getCommentsForResourceAndSendReply(app.mongo.db, { ...request.params }, reply)
  })
}

async function findCommentByIdAndSendReply (db, reply, id) {
  const comment = await Comment.findById(db, id)
  if (!comment) {
    return reply.notFound()
  }
  reply.send(comment)
}

async function getCommentsForResourceAndSendReply (db, refersTo, reply) {
  const comments = await Comment.findRefferingTo(db, refersTo)
  if (!comments) {
    return reply.notFound(`${refersTo.resource} with id ${refersTo.resourceId} does not exist.`)
  }
  reply.send(comments)
}
