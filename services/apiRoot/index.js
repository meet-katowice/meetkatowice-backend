'use strict'

const schema = require('./schema')

module.exports = async function (app, opts) {
  app.get('/', { schema }, async function (request, reply) {
    reply
      .type('application/json')
      .send({
        api: 'meetkatowice_api',
        name: 'MeetKatowice.eu API',
        version: 1
      })
  })
}

module.exports.autoPrefix = '/api'
