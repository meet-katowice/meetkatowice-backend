'use strict'

module.exports.schema = {
  response: {
    200: {
      type: 'object',
      properties: {
        user: {
          type: 'object',
          properties: {
            api: {
              type: 'string'
            },
            name: {
              type: 'string'
            },
            version: {
              type: 'integer'
            }
          }
        }
      }
    }
  }
}
