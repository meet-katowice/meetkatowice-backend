'use strict'

module.exports = async function (app, opts) {
  require('./facebook')(app, opts)
}

module.exports.autoPrefix = '/api/auth'
