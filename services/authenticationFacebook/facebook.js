'use strict'

const schema = require('./schema')

module.exports = async function (app, opts) {
  app.addHook('preHandler', app.authFacebook)
  app.post('/facebook', { schema: schema.authSchema }, async function (request, reply) {
    const user = await getUserByFacebookId(app.mongo.db, request.body.facebookUserId)
    delete user.password
    delete user.email

    if (user.deleted) {
      return reply.forbidden('Account is deleted')
    }

    request.session.user = user
    reply.send(user)
  })
}

async function getUserByFacebookId (db, facebookId) {
  return db.collection('users').findOne({ $and: [
    { 'facebook.id': { $exists: true } },
    { 'facebook.id': facebookId }
  ] })
}
