module.exports = {
  authSchema: {
    body: {
      type: 'object',
      properties: {
        access_token: {
          type: 'string'
        }
      },
      required: ['access_token'],
      additionalProperties: false
    },
    response: {
      200: require('../user/schema/read').userBodySchema
    }
  }
}
