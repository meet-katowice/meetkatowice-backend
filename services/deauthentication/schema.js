'use strict'

module.exports.schema = {
  response: {
    200: {
      type: 'object',
      properties: {
        loggedOut: 'boolean'
      }
    }
  }
}
