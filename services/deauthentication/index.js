'use strict'

const schema = require('./schema')

module.exports = async function (app, opts) {
  app.addHook('preHandler', function (request, reply, next) {
    const sessionId = request.session.sessionId
    delete request.session.sessionId
    request.sessionStore.destroy(sessionId, next)
  })

  app.post('/auth/logout', { schema }, async function (request, reply) {
    reply.send({ loggedOut: true })
  })
}

module.exports.autoPrefix = '/api'
