const ObjectId = require('mongodb').ObjectID
const Attraction = require('../attraction/Attraction')
const User = require('../user/User')
const Comment = require('../comment/Comment')
const collection = 'events'

async function validateEvent (db, event) {
  const attractionsAreValid = await validateAttractions(db, event)
  if (attractionsAreValid !== true) throw new Error(attractionsAreValid)

  return true
}
module.exports.validateEvent = validateEvent

async function validateAttractions (db, event) {
  if (!event.attractionIds) return true

  const attractions = await Attraction.findByIds(db, event.attractionIds)
  if (attractions.length !== event.attractionIds.length) return Error('Invalid list of attractions')

  return true
}

async function createEvent (db, event, authorId) {
  event.authorId = authorId
  event.added = new Date()
  event.participantIds = [authorId]
  const result = await db.collection(collection).insertOne(event)
  return result
}
module.exports.create = createEvent

async function findById (db, id) {
  const event = await db.collection(collection).findOne({ _id: ObjectId(id) })
  const resolvingResults = (await resolveEvents(db, [event]))[0]
  event.author = resolvingResults.author
  event.attractions = resolvingResults.attractions
  event.participants = resolvingResults.participants
  event.comments = resolvingResults.comments

  return event
}
module.exports.findById = findById

async function getAll (db) {
  const events = await db.collection(collection).find({}).toArray()
  const resolvingResults = await resolveEvents(db, events)

  events.forEach((event, index) => {
    event.author = resolvingResults[index].author
    event.attractions = resolvingResults[index].attractions
    event.participants = resolvingResults[index].participants
    event.comments = resolvingResults[index].comments
  })

  return events
}
module.exports.getAll = getAll

async function update (db, event, id) {
  const eventToUpdate = { ...event }
  delete eventToUpdate.attractions
  delete eventToUpdate.participants
  delete eventToUpdate.comments
  delete eventToUpdate.author
  return db.collection(collection).updateOne({ _id: ObjectId(id) }, { $set: { ...eventToUpdate } })
}
module.exports.update = update

async function remove (db, id) {
  await db.collection('events').deleteOne({ _id: ObjectId(id) })
  await db.collection('comments').deleteMany({ 'refersTo.resourceId': ObjectId(id) })
}
module.exports.remove = remove

async function joinToEvent (db, eventId, userId, joinIn) {
  const event = await findById(db, eventId)
  if (!event) return null

  const participants = [...event.participantIds]
  if (joinIn) {
    if (!participants.find((participant) => ObjectId(userId))) {
      participants.push(ObjectId(userId))
    } else {
      return true
    }
  } else participants.splice(participants.indexOf(ObjectId(userId)), 1)

  const updatedEvent = { ...event, participantIds: participants }

  await update(db, updatedEvent, eventId)
  return true
}
module.exports.joinToEvent = joinToEvent

async function resolveEvents (db, events) {
  const tasks = events.map((event) => new Promise((resolve, reject) => {
    Promise.all([
      User.findById(db, event.authorId),
      Attraction.findByIds(db, event.attractionIds),
      User.findByIds(db, event.participantIds),
      Comment.findRefferingTo(db, { resource: 'event', resourceId: event._id })
    ])
      .then((results) => resolve({
        author: results[0],
        attractions: results[1],
        participants: results[2],
        comments: results[3]
      }))
  }))
  return Promise.all(tasks)
}
