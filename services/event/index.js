module.exports = async function (app, opts) {
  app.register(require('./createHandler'))
  app.register(require('./readHandler'))
  app.register(require('./updateHandler'))
  app.register(require('./joinHandler'))
  app.register(require('./deleteHandler'))
}

module.exports.autoPrefix = '/api/event'
