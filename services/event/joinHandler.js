const schema = require('./schema')
const Event = require('./Event')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'events', requiredPermissions: [] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.post('/:eventId/join', { schema: schema.joinEventSchema }, async function (request, reply) {
      await joinToEventAndSendReply(app.mongo.db, reply, request.params.eventId, request.session.user._id, request.body.join)
    })
  })
}

async function joinToEventAndSendReply (db, reply, eventId, userId, joinIn) {
  const joining = await Event.joinToEvent(db, eventId, userId, joinIn)
  if (!joining) return reply.notFound()

  reply.code(200).send(true)
}
