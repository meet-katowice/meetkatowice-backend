const schema = require('./schema')
const Event = require('./Event')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'events', requiredPermissions: ['D'] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.delete('/:eventId', { schema: schema.deleteEventSchema }, async function (request, reply) {
      await deleteEventAndSendReply(app.mongo.db, reply, request.params.eventId)
    })
  })
}

async function deleteEventAndSendReply (db, reply, id) {
  const event = await Event.findById(db, id)
  if (!event) {
    return reply.notFound()
  }

  await Event.remove(db, id)
  reply.code(200).send({ deleted: true })
}
