const schema = require('./schema')
const Event = require('./Event')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'events', requiredPermissions: ['U'] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.put('/update/:eventId', { schema: schema.updateEventSchema }, async function (request, reply) {
      await updateEventAndSendReply(app.mongo.db, reply, request.body, request.params.eventId)
    })
  })
}

async function updateEventAndSendReply (db, reply, event, id) {
  if (Object.keys(event).length === 0) {
    return reply.badRequest()
  }

  const currentEvent = await Event.findById(db, id)
  if (!currentEvent) {
    return reply.notFound()
  }

  try {
    await Event.validateEvent(db, event)
  } catch (e) {
    reply.code(409)
    return reply.send(e)
  }

  await Event.update(db, event, id)
  reply.code(201).send({ ...currentEvent, ...event, _id: id })
}
