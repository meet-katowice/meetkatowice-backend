const objectIdRegExp = '^[A-Fa-f0-9]{24}$'

module.exports = {
  joinEventSchema: {
    params: {
      eventId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    body: {
      type: 'object',
      properties: {
        join: {
          type: 'boolean'
        }
      },
      additionalProperties: false,
      required: ['join']
    },
    response: {
      200: {
        type: 'boolean'
      }
    }
  }
}
