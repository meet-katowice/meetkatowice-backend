const objectIdRegExp = '^[A-Fa-f0-9]{24}$'

const eventSchema = {
  type: 'object',
  properties: {
    _id: {
      type: 'string'
    },
    name: {
      type: 'string'
    },
    description: {
      type: 'string'
    },
    featured: {
      type: 'integer'
    },
    attractions: {
      type: 'array',
      items: require('../../attraction/schema/read').attractionBodySchema
    },
    authorId: {
      type: 'string'
    },
    author: require('../../user/schema/read').userBodySchema,
    added: {
      type: 'string' // date
    },
    startDate: {
      type: 'string'
    },
    permanent: {
      type: 'boolean'
    },
    moderations: {
      type: 'array',
      items: {
        type: 'object',
        authorId: {
          type: 'string'
        },
        date: {
          type: 'string' // date
        },
        inPlus: {
          type: 'boolean'
        }
      }
    },
    participants: {
      type: 'array',
      items: require('../../user/schema/read').userBodySchema
    },
    comments: {
      type: 'array',
      items: require('../../comment/schema/read').commentBodySchema
    }
  },
  additionalProperties: false
}

const eventsListSchema = {
  type: 'array',
  items: eventSchema
}

module.exports = {
  eventSchema: {
    params: {
      eventId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    response: {
      200: eventSchema
    }
  },
  eventBodySchema: eventSchema,
  eventListSchema: {
    response: {
      200: eventsListSchema
    }
  }
}
