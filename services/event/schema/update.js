const objectIdRegExp = '^[A-Fa-f0-9]{24}$'
const iso8601RegExp = /^\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d(\.\d+)?(([+-]\d\d:\d\d)|Z)?$/i

const updateEventSchema = {
  type: 'object',
  properties: {
    name: {
      type: 'string',
      minLength: 2,
      maxLength: 100
    },
    description: {
      type: 'string',
      minLength: 48,
      maxLength: 4096
    },
    attractionIds: {
      type: 'array',
      items: {
        type: 'string',
        pattern: objectIdRegExp
      },
      minItems: 2,
      maxItems: 24,
      uniqueItems: true
    },
    startDate: {
      type: 'string',
      pattern: iso8601RegExp.source
    }
  },
  additionalProperties: false,
  minProperties: 1
}

module.exports = {
  updateEventSchema: {
    params: {
      eventId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    body: updateEventSchema,
    response: {
      201: require('./read').eventBodySchema
    }
  }
}
