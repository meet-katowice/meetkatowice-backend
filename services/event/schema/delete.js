const objectIdRegExp = '^[A-Fa-f0-9]{24}$'

module.exports = {
  deleteEventSchema: {
    params: {
      eventId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    response: {
      200: require('./read').eventBodySchema
    }
  }
}
