const schema = require('./schema')
const Event = require('./Event')

module.exports = async function (app, opts) {
  app.get('/:eventId', { schema: schema.eventSchema }, async function (request, reply) {
    await findEventByIdAndSendReply(app.mongo.db, reply, request.params.eventId)
  })

  app.get('/', { schema: schema.eventListSchema }, async function (request, reply) {
    await getAllEventsAndSendReply(app.mongo.db, reply)
  })
}

async function findEventByIdAndSendReply (db, reply, id) {
  const event = await Event.findById(db, id)
  if (!event) {
    return reply.notFound()
  }
  reply.send(event)
}

async function getAllEventsAndSendReply (db, reply) {
  const events = await Event.getAll(db)
  reply.send(events)
}
