const schema = require('./schema')
const Event = require('./Event')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'events', requiredPermissions: ['C'] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.post('/add', { schema: schema.createEventSchema }, async function (request, reply) {
      await createEventAndSendReply(app.mongo.db, reply, request.session.user._id, request.body)
    })
  })
}

async function createEventAndSendReply (db, reply, authorId, event) {
  try {
    await Event.validateEvent(db, event)
  } catch (e) {
    reply.code(409)
    return reply.send(e)
  }

  const createdEvent = await Event.create(db, event, authorId)
  reply.code(201).send({ ...event, _id: createdEvent.insertedId })
}
