const schema = require('./schema')
const Attraction = require('./Attraction')

module.exports = async function (app, opts) {
  app.get('/:attractionId', { schema: schema.attractionSchema }, async function (request, reply) {
    await findAttractionByIdAndSendReply(app.mongo.db, reply, request.params.attractionId)
  })

  app.get('/', { schema: schema.attractionListSchema }, async function (request, reply) {
    await getAllAttractionsAndSendReply(app.mongo.db, reply, request.query.resolveRelatedData)
  })
}

async function findAttractionByIdAndSendReply (db, reply, id) {
  const attraction = await Attraction.findById(db, id)
  if (!attraction) {
    return reply.notFound()
  }
  reply.send(attraction)
}

async function getAllAttractionsAndSendReply (db, reply, resolveRelatedData = false) {
  const attractionTypes = await Attraction.getAll(db, resolveRelatedData)
  reply.send(attractionTypes)
}
