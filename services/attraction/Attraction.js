const ObjectId = require('mongodb').ObjectID
const httpClient = require('axios').default
const AttractionType = require('../attractionType/AttractionType')
const Comment = require('../comment/Comment')
const User = require('../user/User')
const collection = 'attractions'

async function validateAttraction (db, attraction) {
  const imagesAreValid = await validateImages(attraction)
  if (imagesAreValid !== true) throw new Error(imagesAreValid)
  const kindIsValid = await validateKind(db, attraction)
  if (kindIsValid !== true) throw new Error(kindIsValid)

  return true
}
module.exports.validateAttraction = validateAttraction

async function validateImages (attraction) {
  if (!attraction.images || attraction.images.length === 0) return true
  const imageUrls = [...attraction.images]

  try {
    await Promise.all(imageUrls.map((url) => httpClient.get(url)))
  } catch (e) {
    return new Error(`Cannot access given images at ${e.config.url} Reason: ${e.response.statusText}`)
  }

  return true
}

async function validateKind (db, attraction) {
  if (!attraction.kindId) return true

  const attractionKind = await AttractionType.findById(db, attraction.kindId)
  if (!attractionKind) return Error('Attraction type with given id does not exist')

  return true
}

async function createAttraction (db, attraction, authorId) {
  attraction.authorId = authorId
  attraction.added = new Date()
  const result = await db.collection(collection).insertOne(attraction)
  return result
}
module.exports.create = createAttraction

async function findById (db, id) {
  const attraction = await db.collection(collection).findOne({ _id: ObjectId(id) })
  if (!attraction) return null

  attraction.author = await User.findById(db, attraction.authorId)
  attraction.kind = await AttractionType.findById(db, attraction.kindId)
  attraction.comments = await Comment.findRefferingTo(db, { resource: 'attraction', resourceId: attraction._id })
  return attraction
}
module.exports.findById = findById

async function findByIds (db, ids) {
  const attractions = await db.collection(collection).find({ _id: { $in: ids.map((id) => ObjectId(id)) } }).toArray()
  const resolvingResults = await resolveAttractions(db, attractions)
  attractions.forEach((attraction, index) => {
    attraction.author = resolvingResults[index].author
    attraction.kind = resolvingResults[index].kind
    attraction.comments = resolvingResults[index].comments
  })
  return attractions
}
module.exports.findByIds = findByIds
async function getAll (db, resolveRelated = true) {
  const attractions = await db.collection(collection).find({}).toArray()
  if (resolveRelated) {
    const resolvingResults = await resolveAttractions(db, attractions)
    attractions.forEach((attraction, index) => {
      attraction.author = resolvingResults[index].author
      attraction.kind = resolvingResults[index].kind
      attraction.comments = resolvingResults[index].comments
    })
  }
  return attractions
}
module.exports.getAll = getAll

async function update (db, attraction, id) {
  return db.collection(collection).updateOne({ _id: ObjectId(id) }, { $set: { ...attraction } })
}
module.exports.update = update

async function remove (db, id) {
  await db.collection('attractions').deleteOne({ _id: ObjectId(id) })
  // TODO: delete all events that have given attraction too
  await db.collection('comments').deleteMany({ 'refersTo.resourceId': ObjectId(id) })
}
module.exports.remove = remove

async function resolveAttractions (db, attractions) {
  const tasks = attractions.map((attraction) => new Promise((resolve, reject) => {
    Promise.all([
      User.findById(db, attraction.authorId),
      AttractionType.findById(db, attraction.kindId),
      Comment.findRefferingTo(db, { resource: 'attraction', resourceId: attraction._id })
    ])
      .then((results) => resolve({
        author: results[0],
        kind: results[1],
        comments: results[2]
      }))
  }))
  return Promise.all(tasks)
}
