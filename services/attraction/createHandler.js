const schema = require('./schema')
const Attraction = require('./Attraction')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'attractions', requiredPermissions: ['C'] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.post('/add', { schema: schema.createAttractionSchema }, async function (request, reply) {
      await createAttractionAndSendReply(app.mongo.db, reply, request.session.user._id, request.body)
    })
  })
}

async function createAttractionAndSendReply (db, reply, authorId, attraction) {
  try {
    await Attraction.validateAttraction(db, attraction)
  } catch (e) {
    reply.code(409)
    return reply.send(e)
  }

  const createdAttraction = await Attraction.create(db, attraction, authorId)
  reply.code(201).send({ ...attraction, _id: createdAttraction.insertedId })
}
