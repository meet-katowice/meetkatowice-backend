const schema = require('./schema')
const Attraction = require('./Attraction')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'attractions', requiredPermissions: ['U'] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.put('/update/:attractionId', { schema: schema.updateAttractionSchema }, async function (request, reply) {
      await updateAttractionAndSendReply(app.mongo.db, reply, request.body, request.params.attractionId)
    })
  })
}

async function updateAttractionAndSendReply (db, reply, attraction, id) {
  if (Object.keys(attraction).length === 0) {
    return reply.badRequest()
  }

  const currentAttractionType = await Attraction.findById(db, id)
  if (!currentAttractionType) {
    return reply.notFound()
  }

  try {
    await Attraction.validateAttraction(db, attraction)
  } catch (e) {
    reply.code(409)
    return reply.send(e)
  }

  await Attraction.update(db, attraction, id)
  reply.code(201).send({ ...currentAttractionType, ...attraction, _id: id })
}
