const schema = require('./schema')
const Attraction = require('./Attraction')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'attractions', requiredPermissions: ['D'] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.delete('/:attractionId', { schema: schema.deleteAttractionSchema }, async function (request, reply) {
      await deleteAttractionAndSendReply(app.mongo.db, reply, request.params.attractionId)
    })
  })
}

async function deleteAttractionAndSendReply (db, reply, id) {
  const attraction = await Attraction.findById(db, id)
  if (!attraction) {
    return reply.notFound()
  }

  await Attraction.remove(db, id)
  reply.code(200).send({ deleted: true })
}
