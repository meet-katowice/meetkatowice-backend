const objectIdRegExp = '^[A-Fa-f0-9]{24}$'

const attractionSchema = {
  type: 'object',
  properties: {
    _id: {
      type: 'string'
    },
    name: {
      type: 'string'
    },
    description: {
      type: 'string'
    },
    images: {
      type: 'array',
      items: {
        type: 'string' // urls
      }
    },
    location: {
      type: 'object',
      properties: {
        lat: {
          type: 'number'
        },
        lng: {
          type: 'number'
        }
      },
      additionalProperties: false
    },
    featured: {
      type: 'integer'
    },
    kindId: {
      type: 'string'
    },
    kind: require('../../attractionType/schema/read').attractionTypeBodySchema,
    authorId: {
      type: 'string'
    },
    author: require('../../user/schema/read').userBodySchema,
    added: {
      type: 'string' // date
    },
    moderations: {
      type: 'array',
      items: {
        type: 'object',
        authorId: {
          type: 'string'
        },
        date: {
          type: 'string' // date
        },
        inPlus: {
          type: 'boolean'
        }
      }
    },
    comments: {
      type: 'array',
      items: require('../../comment/schema/read').commentBodySchema
    }
  },
  additionalProperties: false
}

const attractionListSchema = {
  type: 'array',
  items: attractionSchema
}

module.exports = {
  attractionSchema: {
    params: {
      attractionId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    response: {
      200: attractionSchema
    }
  },
  attractionBodySchema: attractionSchema,
  attractionListSchema: {
    query: {
      resolveRelatedData: {
        type: 'boolean',
        default: true
      }
    },
    response: {
      200: attractionListSchema
    }
  }
}
