const objectIdRegExp = '^[A-Fa-f0-9]{24}$'

const updateAttractionSchema = {
  type: 'object',
  properties: {
    name: {
      type: 'string',
      minLength: 2,
      maxLength: 100
    },
    description: {
      type: 'string',
      minLength: 48,
      maxLength: 4096
    },
    images: {
      type: 'array',
      items: {
        type: 'string', // urls
        pattern: new RegExp(/[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/).source
      },
      maxItems: 10
    },
    location: {
      type: 'object',
      properties: {
        lat: {
          type: 'number',
          minimum: -90,
          maxiumum: 90
        },
        lng: {
          type: 'number',
          minimum: -180,
          maxiumum: 180
        }
      },
      required: ['lat', 'lng'],
      additionalProperties: false
    },
    kindId: {
      type: 'string',
      pattern: objectIdRegExp
    },
    featured: {
      type: 'integer',
      minimum: -1,
      maxiumum: 3
    }
  },
  additionalProperties: false,
  minProperties: 1
}

module.exports = {
  updateAttractionSchema: {
    params: {
      attractionId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    body: updateAttractionSchema,
    response: {
      201: require('./read').attractionBodySchema
    }
  }
}
