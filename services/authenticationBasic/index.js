'use strict'

module.exports = async function (app, opts) {
  require('./basic')(app, opts)
}

module.exports.autoPrefix = '/api/auth'
