'use strict'

const schema = require('./schema')

module.exports = async function (app, opts) {
  app.addHook('preHandler', app.basicAuth)

  app.post('/basic', { schema }, async function (request, reply) {
    const authorizationHeader = request.headers.authorization
    const user = await getUserByAuthorizationHeader(app.mongo.db, authorizationHeader)

    if (user.deleted) {
      return reply.forbidden('Account is deleted')
    }

    request.session.user = user
    return { user }
  })
}

async function getUserByAuthorizationHeader (db, authorizationHeader) {
  const credentials = require('basic-auth')
  const username = credentials.parse(authorizationHeader).name
  const user = await db.collection('users').findOne({ username })
  delete user.password
  return user
}
