const objectIdRegExp = '^[A-Fa-f0-9]{24}$'

const userSchema = {
  type: 'object',
  properties: {
    _id: {
      type: 'string'
    },
    username: {
      type: 'string'
    },
    blocked: {
      type: 'boolean'
    },
    deleted: {
      type: 'boolean'
    }
  },
  additionalProperties: false
}

const userListSchema = {
  type: 'array',
  items: userSchema
}

module.exports = {
  userSchema: {
    params: {
      userId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    response: {
      200: userSchema
    }
  },
  userBodySchema: userSchema,
  userListSchema: {
    response: {
      200: userListSchema
    }
  }
}
