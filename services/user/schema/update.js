const objectIdRegExp = '^[A-Fa-f0-9]{24}$'

const updateUserSchema = {
  type: 'object',
  properties: {
    username: {
      type: 'string',
      maxLength: 100,
      minLength: 2
    },
    password: {
      type: 'string',
      minLength: 12,
      maxLength: 128
    },
    permissions: {
      type: 'object',
      properties: {
        users: {
          type: 'array',
          items: {
            type: 'string',
            pattern: '^(C|R|U|D)$'
          }
        },
        attractions: {
          type: 'array',
          items: {
            type: 'string',
            pattern: '^(C|R|U|D)$'
          }
        },
        attractionTypes: {
          type: 'array',
          items: {
            type: 'string',
            pattern: '^(C|R|U|D)$'
          }
        },
        comments: {
          type: 'array',
          items: {
            type: 'string',
            pattern: '^(C|R|U|D)$'
          }
        },
        events: {
          type: 'array',
          items: {
            type: 'string',
            pattern: '^(C|R|U|D)$'
          }
        }
      },
      additionalProperties: false
    }
  },
  additionalProperties: false,
  minProperties: 1
}

module.exports = {
  updateUserSchema: {
    params: {
      userId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    body: updateUserSchema,
    response: {
      201: require('./read').userBodySchema
    }
  }
}
