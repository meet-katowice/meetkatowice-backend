const objectIdRegExp = '^[A-Fa-f0-9]{24}$'

module.exports = {
  deleteUserSchema: {
    params: {
      userId: {
        type: 'string',
        pattern: objectIdRegExp
      }
    },
    response: {
      200: {
        type: 'object',
        properties: {
          deleted: {
            type: 'boolean'
          }
        }
      }
    }
  }
}
