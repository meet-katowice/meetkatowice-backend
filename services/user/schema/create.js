const createUserSchema = {
  type: 'object',
  properties: {
    username: {
      type: 'string',
      maxLength: 100,
      minLength: 2
    },
    password: {
      type: 'string',
      minLength: 12,
      maxLength: 128
    },
    permissions: {
      type: 'object',
      properties: {
        users: {
          type: 'array',
          items: {
            type: 'string',
            pattern: '^(C|R|U|D)$'
          }
        },
        attractions: {
          type: 'array',
          items: {
            type: 'string',
            pattern: '^(C|R|U|D)$'
          }
        },
        attractionTypes: {
          type: 'array',
          items: {
            type: 'string',
            pattern: '^(C|R|U|D)$'
          }
        },
        comments: {
          type: 'array',
          items: {
            type: 'string',
            pattern: '^(C|R|U|D)$'
          }
        },
        events: {
          type: 'array',
          items: {
            type: 'string',
            pattern: '^(C|R|U|D)$'
          }
        }
      },
      additionalProperties: false
    }
  },
  additionalProperties: false,
  required: ['username', 'password', 'permissions']
}

module.exports = {
  createUserSchema: {
    body: createUserSchema,
    response: {
      201: require('./read').userBodySchema
    }
  }
}
