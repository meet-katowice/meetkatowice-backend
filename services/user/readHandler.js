const schema = require('./schema')
const User = require('./User')

module.exports = async function (app, opts) {
  app.get('/:userId', { schema: schema.userSchema }, async function (request, reply) {
    await findAttractionByIdAndSendReply(app.mongo.db, reply, request.params.userId)
  })

  app.get('/', { schema: schema.userListSchema }, async function (request, reply) {
    await getAllUsersAndSendReply(app.mongo.db, reply)
  })
}

async function findAttractionByIdAndSendReply (db, reply, id) {
  const user = await User.findById(db, id)
  if (!user) {
    return reply.notFound()
  }
  reply.send(user)
}

async function getAllUsersAndSendReply (db, reply) {
  const users = await User.getAll(db)
  reply.send(users)
}
