const ObjectId = require('mongodb').ObjectID
const bcrypt = require('bcryptjs')

const collection = 'users'

async function validateUser (db, user) {
  const duplicateUser = await db.collection(collection).findOne({ username: user.username })
  if (duplicateUser) {
    throw new Error('Username is already taken')
  }

  return true
}
module.exports.validateUser = validateUser

async function create (db, user) {
  user.password = await hash(user.password)
  const result = await db.collection(collection).insertOne(user)
  return result
}
module.exports.create = create

function hash (password) {
  return new Promise((resolve, reject) => {
    bcrypt.genSalt(10, (error, salt) => {
      if (error) reject(error)
      bcrypt.hash(password, salt, (error, hash) => {
        if (error) reject(error)
        resolve(hash)
      })
    })
  })
}

async function findById (db, id) {
  return db.collection(collection).findOne({ _id: ObjectId(id) })
}
module.exports.findById = findById

async function findByIds (db, ids) {
  if (!ids) return []
  return db.collection(collection).find({ _id: { $in: ids.map((id) => ObjectId(id)) } }).toArray()
}
module.exports.findByIds = findByIds

async function getAll (db) {
  return db.collection(collection).find({}).toArray()
}
module.exports.getAll = getAll

async function update (db, user, id) {
  if (user.password) user.password = await hash(user.password)
  return db.collection(collection).updateOne({ _id: ObjectId(id) }, { $set: { ...user } })
}
module.exports.update = update

async function remove (db, id) {
  await db.collection('attractions').deleteMany({ authorId: ObjectId(id) })
  await db.collection(collection).updateOne({ _id: ObjectId(id) },
    { $set: { deleted: true, username: 'Deleted' } })
  await db.collection('sessions').deleteMany({ 'session.user._id': ObjectId(id) })
}
module.exports.remove = remove
