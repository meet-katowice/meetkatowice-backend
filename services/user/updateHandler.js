const schema = require('./schema')
const User = require('./User')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'users', requiredPermissions: ['U'] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.put('/update/:userId', { schema: schema.updateUserSchema }, async function (request, reply) {
      await updateUserAndSendReply(app.mongo.db, reply, request.body, request.params.userId)
    })
  })
}

async function updateUserAndSendReply (db, reply, user, id) {
  if (Object.keys(user).length === 0) {
    return reply.badRequest()
  }

  const currentUser = await User.findById(db, id)
  if (!currentUser) {
    return reply.notFound()
  }

  try {
    await User.validateUser(db, user)
  } catch (e) {
    reply.code(409)
    return reply.send(e)
  }

  await User.update(db, user, id)
  reply.code(201).send({ ...currentUser, ...user, _id: id })
}
