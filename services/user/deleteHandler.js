const schema = require('./schema')
const User = require('./User')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'users', requiredPermissions: ['D'] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.delete('/:userId', { schema: schema.deleteUserSchema }, async function (request, reply) {
      await deleteUserAndSendReply(app.mongo.db, reply, request.params.userId)
    })
  })
}

async function deleteUserAndSendReply (db, reply, id) {
  const user = await User.findById(db, id)
  if (!user || user.deleted) {
    return reply.notFound()
  }

  await User.remove(db, id)
  reply.code(200).send({ deleted: true })
}
