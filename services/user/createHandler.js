const schema = require('./schema')
const User = require('./User')

module.exports = async function (app, opts) {
  app.register(
    require('../../plugins/permissions'),
    { resource: 'users', requiredPermissions: ['C'] }
  )

  app.register(async (instance, opts) => {
    instance.addHook('preHandler', instance.checkPermissions)

    instance.post('/add', { schema: schema.createUserSchema }, async function (request, reply) {
      await createUserAndSendReply(app.mongo.db, reply, request.body)
    })
  })
}

async function createUserAndSendReply (db, reply, user) {
  try {
    await User.validateUser(db, user)
  } catch (e) {
    reply.code(409)
    return reply.send(e)
  }

  const createdUser = await User.create(db, user)
  reply.code(201).send({ ...user, _id: createdUser.insertedId })
}
