'use strict'

const schema = require('./schema')

module.exports = async function (app, opts) {
  app.addHook('preHandler', app.authGoogle)
  app.post('/google', { schema: schema.authSchema }, async function (request, reply) {
    const user = await getUserByGoogleId(app.mongo.db, request.body.googleUserId)
    delete user.password
    delete user.email

    if (user.deleted) {
      return reply.forbidden('Account is deleted')
    }

    request.session.user = user
    reply.send(user)
  })
}

async function getUserByGoogleId (db, googleId) {
  return db.collection('users').findOne({ 'google.id': googleId })
}
