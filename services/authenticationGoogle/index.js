'use strict'

module.exports = async function (app, opts) {
  require('./google')(app, opts)
}

module.exports.autoPrefix = '/api/auth'
