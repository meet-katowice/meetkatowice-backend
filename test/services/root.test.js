'use strict'

const { test } = require('tap')
const { build } = require('../helper')

test('default root route', async (t) => {
  const app = build(t)

  // Read the .env file.
  require('dotenv').config()

  const res = await app.inject({
    url: '/'
  })
  t.deepEqual(JSON.parse(res.payload), {
    api: 'fastify_api_boilerplate',
    name: 'Fastify API Boilerplate',
    version: 1
  })
})
