FROM node:12

ENV TZ=Europe/Warsaw
COPY ./package.json /opt/meetkatowice-backend/
COPY ./package-lock.json /opt/meetkatowice-backend/

WORKDIR /opt/meetkatowice-backend
RUN npm install

ADD . /opt/meetkatowice-backend

EXPOSE 3000

CMD [ "npm", "start" ]
