const bcrypt = require('bcrypt')

prompt(`What's the password to hash? :`, function (password) {
  console.info(bcrypt.hashSync(password, 10))
  process.exit(0)
})

function prompt (question, callback) {
  const stdin = process.stdin
  const stdout = process.stdout

  stdin.resume()
  stdout.write(question)

  stdin.once('data', function (data) {
    callback(data.toString().trim())
  })
}
