'use strict'

const path = require('path')
const AutoLoad = require('fastify-autoload')

module.exports = function (app, opts, next) {
  // Do not touch the following lines

  // This loads all plugins defined in plugins
  // those should be support plugins that are reused
  // through your application
  app.register(AutoLoad, {
    dir: path.join(__dirname, 'plugins'),
    options: Object.assign({}, opts)
  })

  require('./plugins/basicAuth')(app, opts)

  // This loads all plugins defined in services
  // define your routes in one of these
  app.register(AutoLoad, {
    dir: path.join(__dirname, 'services'),
    options: Object.assign({}, opts)
  })

  // Make sure to call next when done
  next()
}
