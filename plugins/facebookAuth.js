'use strict'

const httpRequest = require('axios')
const uniqueRandomRange = require('unique-random-range')
const fp = require('fastify-plugin')

/**
 * Facebook authorization plugin.
 * Creates or lookups user in the database (thus database plugin needs to be registered
 * before this plugin). Requires Facebook user's id, name and email.
 * Email is not required for this application, so when user declines access
 * to his email address all profile checks will pass.
 * Email can be filled up by the user later or not at all.
 */
module.exports = fp(async function (app, opts) {
  app.decorate('authFacebook', authFacebook)

  function authFacebook (request, reply, next) {
    if (!checkRequestBody(request.body)) {
      done(new Error('Invalid or no access_token field in request body'))
      return
    }
    getUserProfileFromFacebook(request.body.access_token)
      .then(function (fbProfile) {
        if (!checkUserProfile(fbProfile)) {
          done(new Error('Access token is not valid or does not have proper scope'))
          return
        }

        lookForUserInDatabase(app.mongo.db, fbProfile)
          .then(function (userProfile) {
            if (!userProfile) {
              mergeFbProfileWithUserProfile(app.mongo.db, fbProfile)
                .then(function (merged) {
                  if (!merged) {
                    createUserProfile(app.mongo.db, fbProfile)
                      .then(function (created) {
                        if (!created) {
                          done(new Error('Cannot create user profile'))
                        } else done(undefined, fbProfile.id)
                      })
                  } else {
                    done(undefined, fbProfile.id)
                  }
                })
            } else {
              done(undefined, fbProfile.id)
            }
          })
      })
      .catch(function (error) {
        console.error(error)
        next(error)
      })

    function done (error, facebookUserId) {
      if (error !== undefined) {
        reply.code(401)
        next(error)
      } else {
        request.body.facebookUserId = facebookUserId
        next()
      }
    }
  }
})

function checkRequestBody (requestBody) {
  if (!requestBody ||
    !requestBody.access_token ||
    typeof requestBody.access_token !== 'string' ||
    requestBody.access_token.length < 2) return false
  return true
}

async function getUserProfileFromFacebook (accessToken) {
  const desiredPath = `https://graph.facebook.com/v4.0/me?access_token=${accessToken}&fields=id,name&format=json`
  try {
    const response = await httpRequest.default.get(desiredPath)
    return response.data
  } catch (exception) {
    console.error(exception.response.data)
    return false
  }
}

/**
 * Validates received user profile.
 * Note: email is not required, so checks does not lookup
 * anything related to it.
 * @param {Object} facebookProfile
 */
function checkUserProfile (facebookProfile) {
  if (!facebookProfile ||
    !facebookProfile.id ||
    !facebookProfile.name) return false
  return true
}

async function lookForUserInDatabase (db, facebookProfile) {
  return db.collection('users').findOne({
    $and: [
      { facebook: { $exists: true } },
      { 'facebook.id': facebookProfile.id }
    ]
  })
}

async function mergeFbProfileWithUserProfile (db, facebookProfile) {
  const result = await db.collection('users').updateOne(
    { $and: [
      { email: { $exists: true } },
      { email: facebookProfile.email }
    ] },
    {
      $set: {
        facebook: {
          id: facebookProfile.id
        }
      }
    }
  )

  return result.modifiedCount !== 0
}

async function createUserProfile (db, facebookProfile) {
  const result = await db.collection('users').insertOne({
    username: createUsernameOutOfFullname(facebookProfile.name),
    fullname: facebookProfile.name,
    // email: facebookProfile.email,
    permissions: {
      users: ['R'],
      attractions: ['R'],
      events: ['C', 'R'],
      comments: ['C', 'R'],
      attractionTypes: ['R']
    },
    facebook: {
      id: facebookProfile.id
    }
  })

  return result.insertedCount !== 0
}

/**
 * @param {String} fullname
 */
function createUsernameOutOfFullname (fullname) {
  const rand = uniqueRandomRange(1, 999999)
  let username = fullname.toLowerCase().replace(/[^ -~]+/g, '')
  username = username.replace(/ /g, '_')
  username += `_${rand()}`
  return username
}
