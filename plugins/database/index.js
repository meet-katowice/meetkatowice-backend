const mongoDbUri = process.env.DB_AUTH_URL || `mongodb://${process.env.DB_AUTH_COMBO ? process.env.DB_AUTH_COMBO + '@' : ''}${process.env.DB_ADDRESS}/${process.env.DB_NAME}`

const fp = require('fastify-plugin')

module.exports = fp(async function (app, opts) {
  app.register(require('fastify-mongodb'), {
    forceClose: true,
    url: mongoDbUri
  })
})
