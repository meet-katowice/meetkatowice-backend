const fp = require('fastify-plugin')

module.exports = fp(async function (app, opts) {
  app.register(require('fastify-basic-auth'),
    { validate, authenticate: false })
})

async function validate (username, password, request, reply) {
  const bcrypt = require('bcryptjs')
  const db = this.mongo.db
  const user = await db.collection('users').findOne({ username })
  if (user.deleted) {
    throw new Error('This account was deleted')
  }
  const isPasswordValid = await bcrypt.compare(password, user.password)
  if (user == null || !isPasswordValid) {
    throw new Error('Incorrect username or password')
  }
}

module.exports.autoload = false
