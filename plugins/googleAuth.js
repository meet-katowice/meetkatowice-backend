'use strict'

const httpRequest = require('axios')
const uniqueRandomRange = require('unique-random-range')
const fp = require('fastify-plugin')

/**
 * Facebook authorization plugin.
 * Creates or lookups user in the database (thus database plugin needs to be registered
 * before this plugin). Requires Google user's id, name and email.
 * Email is not required for this application, so when user declines access
 * to his email address all profile checks will pass.
 * Email can be filled up by the user later or not at all.
 */
module.exports = fp(async function (app, opts) {
  app.decorate('authGoogle', authGoogle)

  function authGoogle (request, reply, next) {
    if (!checkRequestBody(request.body)) {
      done(new Error('Invalid or no access_token field in request body'))
      return
    }
    getUserProfileFromGoogle(request.body.access_token)
      .then(function (googleProfile) {
        if (!checkUserProfile(googleProfile)) {
          done(new Error('Access token is not valid or does not have proper scope'))
          return
        }

        lookForUserInDatabase(app.mongo.db, googleProfile)
          .then(function (userProfile) {
            if (!userProfile) {
              mergeGoogleProfileWithUserProfile(app.mongo.db, googleProfile)
                .then(function (merged) {
                  if (!merged) {
                    createUserProfile(app.mongo.db, googleProfile)
                      .then(function (created) {
                        if (!created) {
                          done(new Error('Cannot create user profile'))
                        } else done(undefined, googleProfile.id)
                      })
                  } else {
                    done(undefined, googleProfile.id)
                  }
                })
            } else {
              done(undefined, googleProfile.id)
            }
          })
      })
      .catch(function (error) {
        console.error(error)
        next(error)
      })

    function done (error, googleUserId) {
      if (error !== undefined) {
        reply.code(401)
        next(error)
      } else {
        request.body.googleUserId = googleUserId
        next()
      }
    }
  }
})

function checkRequestBody (requestBody) {
  if (!requestBody ||
    !requestBody.access_token ||
    typeof requestBody.access_token !== 'string' ||
    requestBody.access_token.length < 2) return false
  return true
}

async function getUserProfileFromGoogle (accessToken) {
  const desiredPath = `https://www.googleapis.com/oauth2/v2/userinfo?access_token=${accessToken}&fields=id,name&format=json`
  try {
    const response = await httpRequest.default.get(desiredPath)
    return response.data
  } catch (exception) {
    console.error(exception.response.data)
    return false
  }
}

/**
 * Validates received user profile.
 * Note: email is not required, so checks does not lookup
 * anything related to it.
 * @param {Object} googleProfile
 */
function checkUserProfile (googleProfile) {
  if (!googleProfile ||
    !googleProfile.id ||
    !googleProfile.name) return false
  return true
}

async function lookForUserInDatabase (db, googleProfile) {
  return db.collection('users').findOne({
    google: { $exists: true },
    'google.id': googleProfile.id
  })
}

async function mergeGoogleProfileWithUserProfile (db, googleProfile) {
  const result = await db.collection('users').updateOne(
    { $and: [{ email: { $exists: true } }, { email: googleProfile.email }] },
    {
      $set: {
        google: {
          id: googleProfile.id
        }
      }
    }
  )

  return result.modifiedCount !== 0
}

async function createUserProfile (db, googleProfile) {
  const result = await db.collection('users').insertOne({
    username: createUsernameOutOfFullname(googleProfile.name),
    fullname: googleProfile.name,
    // email: googleProfile.email,
    permissions: {
      users: ['R'],
      attractions: ['R'],
      events: ['C', 'R'],
      comments: ['C', 'R'],
      attractionTypes: ['R']
    },
    google: {
      id: googleProfile.id
    }
  })

  return result.insertedCount !== 0
}

/**
 * @param {String} fullname
 */
function createUsernameOutOfFullname (fullname) {
  const rand = uniqueRandomRange(1, 999999)
  let username = fullname.toLowerCase().replace(/[^ -~]+/g, '')
  username = username.replace(/ /g, '_')
  username += `_${rand()}`
  return username
}
