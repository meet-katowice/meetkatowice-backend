const mongoDbUri = process.env.DB_AUTH_URL || `mongodb://${process.env.DB_AUTH_COMBO ? process.env.DB_AUTH_COMBO + '@' : ''}${process.env.DB_ADDRESS}/${process.env.DB_NAME}`

const fp = require('fastify-plugin')

module.exports = fp(async function (app, opts) {
  app.register(require('fastify-cookie'))
  const sessionPlugin = require('fastify-session')
  const MongoDBStore = require('connect-mongodb-session')(sessionPlugin)
  app.register(sessionPlugin, {
    secret: process.env.SESSION_SECRET,
    saveUninitialized: false,
    store: new MongoDBStore({
      uri: mongoDbUri,
      collection: 'sessions'
    }),
    cookie: {
      secure: false
    }
  })
})
