'use strict'

const fp = require('fastify-plugin')
const ObjectId = require('objectid')

async function checkIfUserIsResourceOwner (db, user, resource, resourceId) {
  if (user._id.toString() === resourceId.toString()) return true
  const requiredResource = await db.collection(resource).findOne({ _id: ObjectId(resourceId) }, { authorId: 1 })
  if (!requiredResource) return false
  return requiredResource.authorId && requiredResource.authorId.toString() === user._id.toString()
}

/**
 * Permissions handling plugin.
 * Users does have CRUD permissions for known resources, like attractions or comments.
 * This plugin guards access to given resource by checking if
 * requesting user have proper permissions to perform requested actions.
 *
 * Remeber, that if user is an owner of given resource, like comment,
 * it can CRUD it regardless of specified requiredPermissions. Ownership of
 * given resource is determined by loading the resource by id and
 * comparing resource.authorId to requesting user _id.
 *
 * This plugin checks if user is logged in (it loads user info from request.session)
 * and if given account is deleted.
 * If you just want to make sure that user is logged in, pass empty array
 * as requiredPermissions parameter in opts.
 */
module.exports = fp(async function (app, opts) {
  const { resource, requiredPermissions } = opts
  if (typeof resource !== 'string') throw Error(`Error while registering plugin: 'resource' must be a string. Got: ${typeof resource}`)
  if (!Array.isArray(requiredPermissions)) throw Error(`Error while registering plugin: 'requiredPermissions' must be an array. Got: ${typeof requiredPermissions}`)

  const userTriesToModifyResource = requiredPermissions.includes('U') || requiredPermissions.includes('D')

  app.decorate('checkPermissions', async function (request, reply) {
    const { user } = request.session
    if (!user || !user._id) {
      reply.code(401)
      throw new Error('Not logged in user tried to access private realm')
    }
    if (user.deleted) {
      reply.code(403)
      throw new Error('Deleted user tries to access private realm')
    }
    if (requiredPermissions.length === 0) return

    const { id, userId, eventId, attractionId, commentId } = request.params
    const resourceId = id || userId || eventId || attractionId || commentId
    if (userTriesToModifyResource && resourceId) {
      const isUserResourceOwner = await checkIfUserIsResourceOwner(app.mongo.db, user, resource, resourceId)
      if (isUserResourceOwner) return
    }

    const userPermissionsForResource = request.session.user.permissions[resource] || []
    let containsAllRequiredPermissions = true
    requiredPermissions.forEach((permission) => {
      if (!userPermissionsForResource.includes(permission)) containsAllRequiredPermissions = false
    })
    if (!containsAllRequiredPermissions) {
      reply.code(403)
      throw new Error(`User without proper permissions tried to access restricted resource. Required: ${requiredPermissions}; User's: ${user.permissions[resource] || 'none'}`)
    }
  })
})
module.exports.autoload = false
