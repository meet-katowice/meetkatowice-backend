const https = require('https')
const httpsAgent = new https.Agent({
  rejectUnauthorized: false
})
const httpClient = require('axios').create()
const pathLib = require('path')
const fs = require('fs')
const prompt = require('prompt')
const AdmZip = require('adm-zip')

const apiUrl = 'https://meetkatowice.eu/api'

function getArchiveUrl (archiveName) {
  return `https://otwartezabytki.pl/history/${archiveName}`
}

prompt.start()
prompt.get(['archiveName', 'sessionId'], async function (err, { archiveName, sessionId }) {
  if (err) {
    throw new Error(err)
  }

  const url = getArchiveUrl(archiveName)
  // await download(url);
  const zip = new AdmZip('relics.zip')
  const relics = []
  for (const { entryName } of zip.getEntries()) {
    if (entryName.includes('.json')) {
      const text = zip.readAsText(entryName)
      const relicJson = JSON.parse(text)
      if (isInKatowice(relicJson.latitude, relicJson.longitude, 0.04)) {
        relics.push(relicJson)
      }
    }
  }
  console.log({ relic: relics.find((relic) => relic.photos.length > 0) })
  console.log({ relics: relics.map((relic) => convertOzRelictToMk(relic)).filter((relic) => relic.images.length > 0)[0].images })

  const typesWithIds = await ensureRelicTypes(sessionId, getUniqueCategoriesFromList(relics))

  for await (const relic of relics) {
    const convertedRelic = convertOzRelictToMk(relic)
    const kindId = typesWithIds.find((type) => type.name === convertedRelic.kind.name)._id
    await addRelicToDb(sessionId, { ...convertedRelic, kindId })
  }
})

async function download (url) {
  const path = pathLib.resolve(__dirname, 'relics.zip')
  const writer = fs.createWriteStream(path)

  const response = await httpClient({
    url,
    method: 'GET',
    responseType: 'stream'
  })

  response.data.pipe(writer)

  return new Promise((resolve, reject) => {
    writer.on('finish', resolve)
    writer.on('error', reject)
  })
}

const katoLat = 50.270908
const katoLng = 19.039993
function isInKatowice (lat, lng, tolerance = 0.09) {
  return (
    lat + tolerance > katoLat && lat - tolerance < katoLat
  ) && (
    lng + tolerance > katoLng && lng - tolerance < katoLng
  )
}

function getRelicCategory (relic) {
  return relic.categories ? relic.categories[0] ? relic.categories[0] : 'nieznany' : 'nieznany'
}

function getCategoryNameFromCategory (category = '') {
  let categoryName = category.toString()
  categoryName = categoryName.replace(/_/gi, ' ')
  categoryName = categoryName.substring(0, 1).toUpperCase() + categoryName.substring(1).toLowerCase()
  return categoryName
}

function getUniqueCategoriesFromList (list) {
  const categories = []
  for (const relic of list) {
    const kind = getRelicCategory(relic)
    if (!categories.some((category) => category.kind === kind)) {
      categories.push({
        name: getCategoryNameFromCategory(kind),
        kind
      })
    }
  }

  return categories
}

function convertOzRelictToMk (ozRelic) {
  return {
    name: ozRelic.identification,
    oz_id: ozRelic.id,
    location: {
      lat: ozRelic.latitude,
      lng: ozRelic.longitude
    },
    images: ozRelic.photos.map((photo) => `https://otwartezabytki.pl${photo.file.maxi.url}`),
    kind: {
      name: getCategoryNameFromCategory(getRelicCategory(ozRelic)),
      kind: getRelicCategory(ozRelic)
    },
    description: `\n${ozRelic.street}\n${ozRelic.description}\n${ozRelic.dating_of_obj} - ${ozRelic.register_number} - Id w API otwartezabytki.pl: ${ozRelic.id}`
  }
}

async function ensureRelicTypes (sessionId, types) {
  const typesWithId = []
  const knownTypes = (await httpClient.get(`${apiUrl}/attractionType`)).data
  for await (const type of types) {
    const isKnown = knownTypes.find((knownType) => knownType.name === type.name)
    if (isKnown) {
      typesWithId.push(isKnown)
      continue
    }

    const result = await httpClient.post(`${apiUrl}/attractionType/add`, type, {
      headers: {
        Cookie: `sessionId=${sessionId}`
      },
      httpsAgent
    })
    typesWithId.push(result.data)
  }

  return typesWithId
}

async function addRelicToDb (sessionId, relic) {
  try {
    await httpClient.post(`${apiUrl}/attraction/add`, relic, {
      headers: {
        Cookie: `sessionId=${sessionId}`
      },
      httpsAgent
    })
  } catch (e) {
    console.log({ e: e.response.data })
  }
}

// 1. Pobierz zipa (zapytaj użytkownika o nazwę)
// 2. Rozpakuj zipa
// 3. Wczytaj dane do tablicy
// 4. Przefiltruj zabytki z Katowic
// 5. Konwertuj do struktury api
// 6. Pobierz zabytki z bazy danych, które mają oz_id
// 7.1 Jeżeli !oz_id w bazie, dodaj do bazy
// 7.2 Jeżeli oz_id w bazie, porównaj api z oz
// 7.2.1 Jeżeli api != oz, podmień w api dane na nowe z oz
// 7.2.2 Jeżeli takie same, nic nie rób
